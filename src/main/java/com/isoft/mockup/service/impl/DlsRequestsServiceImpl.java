package com.isoft.mockup.service.impl;

import com.isoft.mockup.service.DlsRequestsService;
import com.isoft.mockup.domain.DlsRequests;
import com.isoft.mockup.repository.DlsRequestsRepository;
import com.isoft.mockup.service.dto.*;
import com.isoft.mockup.service.mapper.DlsRequestsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link DlsRequests}.
 */
@Service
@Transactional
public class DlsRequestsServiceImpl implements DlsRequestsService {

    private final Logger log = LoggerFactory.getLogger(DlsRequestsServiceImpl.class);

    private final DlsRequestsRepository dlsRequestsRepository;

    private final DlsRequestsMapper dlsRequestsMapper;

    public DlsRequestsServiceImpl(DlsRequestsRepository dlsRequestsRepository, DlsRequestsMapper dlsRequestsMapper) {
        this.dlsRequestsRepository = dlsRequestsRepository;
        this.dlsRequestsMapper = dlsRequestsMapper;
    }

    /**
     * Save a dlsRequests.
     *
     * @param dlsRequestsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DlsRequestsDTO save(DlsRequestsDTO dlsRequestsDTO) {
        log.debug("Request to save DlsRequests : {}", dlsRequestsDTO);
        DlsRequests dlsRequests = dlsRequestsMapper.toEntity(dlsRequestsDTO);
        dlsRequests = dlsRequestsRepository.save(dlsRequests);
        return dlsRequestsMapper.toDto(dlsRequests);
    }

    /**
     * Get all the dlsRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DlsRequestsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DlsRequests");
        return dlsRequestsRepository.findAll(pageable)
            .map(dlsRequestsMapper::toDto);
    }


    /**
     * Get one dlsRequests by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DlsRequestsDTO> findOne(Long id) {
        log.debug("Request to get DlsRequests : {}", id);
        return dlsRequestsRepository.findById(id)
            .map(dlsRequestsMapper::toDto);
    }

    /**
     * Delete the dlsRequests by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DlsRequests : {}", id);
        dlsRequestsRepository.deleteById(id);
    }

    @Override
    public InquireExamEligibilityResponse inquiry(String nationalId, String passportKey, String requestId, String countryCode) {
        InquireExamEligibilityResponse response = new InquireExamEligibilityResponse();

        ThreeSHeader header = new ThreeSHeader();
        header.setVersion("1.0");
        header.setCategory("response");
        header.setService("TIT_TheoExam_Inquiry");
        header.setTimestamp(new Date().toString());
        header.setTid(UUID.randomUUID().toString());

        response.setHeader(header);

        InquireExamEligibilityResponseBody body = new InquireExamEligibilityResponseBody();
        body.setErrorMessage(200);


        List<DlsRequests> dlsRequestsList = null;   // final result of requests

        if (requestId != null && !requestId.trim().equals("")) { // not empty and not null
            log.debug("Searching by RequestNo... ");
            dlsRequestsList = dlsRequestsRepository.findByRequestID(requestId);
        } else if (nationalId != null && !nationalId.trim().equals("")) { // not empty and not null
            log.debug("Searching by NationalId... ");
            dlsRequestsList = dlsRequestsRepository.findByNationalID(nationalId);
        } else {
            log.debug("Searching by PassportKey and CountryCode... ");
            dlsRequestsList = dlsRequestsRepository.findByPassportNoAndPassportIssueCountry(passportKey, countryCode);
        }

        List<InquireExamEligibilityResponseDetails> allRequests = convertToReqDetails(dlsRequestsList);
        body.setRequests(allRequests);
        response.setBody(body);
        return response;
    }

    private List<InquireExamEligibilityResponseDetails> convertToReqDetails(List<DlsRequests> dlsRequestsList) {

        List<InquireExamEligibilityResponseDetails> output = new ArrayList<>();

        for (DlsRequests currentRequest : dlsRequestsList) {
            InquireExamEligibilityResponseDetails reqDetails = new InquireExamEligibilityResponseDetails();

            reqDetails.setRequestID(currentRequest.getRequestID());
            reqDetails.setFirstName(currentRequest.getFirstName());
            reqDetails.setFamilyName(currentRequest.getFamilyName());
            reqDetails.setMiddleName(currentRequest.getMiddleName());
            reqDetails.setLastName(currentRequest.getLastName());
            reqDetails.setBirthDate(currentRequest.getBirthDate());
            reqDetails.setTrafficUnit(currentRequest.getTrafficUnit());
            reqDetails.setLicenseType(currentRequest.getLicenseType());
            reqDetails.setNationalID(currentRequest.getNationalID());
            reqDetails.setPassportIssueCountry(currentRequest.getPassportIssueCountry());
            reqDetails.setPassportNo(currentRequest.getPassportNo());
            reqDetails.setQueueNumber(currentRequest.getQueueNumber());
            output.add(reqDetails);
        }

        return output;
    }
}
