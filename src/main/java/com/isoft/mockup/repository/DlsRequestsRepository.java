package com.isoft.mockup.repository;

import com.isoft.mockup.domain.DlsRequests;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the DlsRequests entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DlsRequestsRepository extends JpaRepository<DlsRequests, Long>, JpaSpecificationExecutor<DlsRequests> {

    List<DlsRequests> findByRequestID(String requestId);

    List<DlsRequests> findByNationalID(String nationalId);

    List<DlsRequests> findByPassportNoAndPassportIssueCountry(String passportNo, String passportIssueCountry);


}
